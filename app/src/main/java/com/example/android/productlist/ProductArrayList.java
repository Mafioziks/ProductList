package com.example.android.productlist;

import java.util.ArrayList;

public class ProductArrayList extends ArrayList<Product> {
    public boolean contains(Object o) {
        if (o.getClass() == (new Product(0)).getClass()) {
            for (int i = 0; i < size(); i++) {
                if (((Product) o).getId() == get(i).getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get product by product ID not by order id in ArrayList
     *
     * @param id int
     * @return Product
     */
    public Product getById(int id) {
        for (int i = 0; i < size(); i++) {
            if (get(i).getId() == id) {
                return get(i);
            }
        }
        return null;
    }
}
