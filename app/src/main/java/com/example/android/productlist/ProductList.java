package com.example.android.productlist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.Iterator;

public class ProductList extends AppCompatActivity implements RestView {

    private ProductArrayList products = new ProductArrayList();
    private LCBOClient api = new LCBOClient(this);
    ProductAdapter prodAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        ListView rootLayout = (ListView) findViewById(R.id.root_layout);

        if (!isConnected()) {
            (Toast.makeText(this, R.string.internet_turned_off, Toast.LENGTH_SHORT)).show();
            return;
        }

        api.authorize();
        api.setProductArrayList(products);
        api.fetchProducts();

        prodAdapter = new ProductAdapter(this, products);
        rootLayout.setAdapter(prodAdapter);

        rootLayout.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount + 3 > (totalItemCount - firstVisibleItem) || totalItemCount <= 0) {
                    api.fetchProducts();
                }
            }
        });

        rootLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent productPage = new Intent(ProductList.this, ProductPage.class);
                Bundle bundle = new Bundle();
                bundle.putInt("product_id", products.get(position).getId());
                productPage.putExtras(bundle);
                startActivity(productPage);
            }
        });
    }

    /**
     * Check if internet is on and device is connected to it
     *
     * @return boolean
     */
    public boolean isConnected() {
        ConnectivityManager conManager = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conManager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }

        return false;
    }


    /**
     * Refresh view with new added products to adapter
     */
    @Override
    public void refreshView() {
        prodAdapter.notifyDataSetChanged();
    }

    /**
     * Fetch new page of products
     * @param view View
     */
    public void fetchProducts(View view) {
        api.fetchProducts();
    }

    /**
     * On activity resume refresh view with changed data
     */
    @Override
    public void onResume() {
        super.onResume();
        if (prodAdapter != null) {
            refreshView();
        }
    }
}