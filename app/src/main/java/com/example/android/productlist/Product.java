package com.example.android.productlist;

import android.media.Image;

public class Product {
    private int id;
    private String name;
    private String imgUrl;
    private Image img;
    private int price;
    private String description;
    private boolean favorite = false;
    private boolean dead;
    private String[] tags;
    private boolean discontinued;
    private int regularPrice;
    private String primaryCategory;
    private String secondaryCategory;
    private String origin;
    private String productPackage;
    private int totalPackageUnits;
    private String servingSuggestion;
    private String tastingNote;
    private String producer;

    public Product(int id) {
        this.id = id;
    }

    public void setName(String name) { this.name = name; }

    public void setImgUrl(String imgUrl) { this.imgUrl = imgUrl; }

    public void setImg(Image img) { this.img = img; }

    public void setPrice(int price) { this.price = price; }

    public void setDescription(String description) { this.description = description; }

    public void setFavorite(boolean fav) {this.favorite = fav; }

    public int getId() { return id; }

    public String getName() { return name; }

    public String getImgUrl() { return imgUrl; }

    public Image getImg() { return img; }

    public int getPrice() { return price; }

    public String getDescription() { return description != null || description.equals("null")? description : " "; }

    public boolean getFavorite() { return favorite; }

    public void setDead(boolean is_dead) { dead = is_dead; }

    public void addTags(String tags) { this.tags = tags.split("\\s+"); }

    public void setDiscontinued(boolean is_discontinued) { discontinued = is_discontinued; }

    public void setRegularPrice(int regular_price_in_cents) { regularPrice = regular_price_in_cents; }

    public void setCategory(String primary_category) { primaryCategory = primary_category; }

    public void setSecondaryCategory(String secondary_category) { secondaryCategory = secondary_category; }

    public void setOrigin(String origin) { this.origin = origin; }

    public void setPackage(String aPackage) { productPackage = aPackage; }

    public void setTotalPackageUnits(int totalPackageUnits) { this.totalPackageUnits = totalPackageUnits; }

    public void setServingSuggestion(String serving_suggestion) { servingSuggestion = serving_suggestion; }

    public void setTastingNote(String tastingNote) { this.tastingNote = tastingNote; }

    public String getServingSuggestion() { return servingSuggestion; }

    public String getTastingNote() { return tastingNote; }

    public String getPackaging() { return productPackage; }

    public String getPrimaryCategory() { return primaryCategory; }

    public String getSecondaryCategory() { return secondaryCategory; }

    public String getProducer() { return producer; }

    public String getOrigin() { return origin; }

    public void setProducer(String producer) { this.producer = producer; }
}
