package com.example.android.productlist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class Favorites {
    private SQLiteDatabase database;
    private ProductDatabaseHelper helper;
    private String[] allColumns = {
        ProductDatabaseHelper.COLUMN_ID,
        ProductDatabaseHelper.COLUMN_PRODUCT_ID
    };

    /**
     * Initialize Favorited product data manager
     * @param context
     */
    public Favorites(Context context) {
        helper = new ProductDatabaseHelper(context);
    }

    /**
     * Open database to write data
     *
     * @throws SQLException
     */
    public void openToWrite() throws SQLException {
        if (database == null) {
            database = helper.getWritableDatabase();
        }
    }

    /**
     * Open database to read
     *
     * @throws SQLException
     */
    public void openToRead() throws SQLException {
        if (database == null) {
            database = helper.getReadableDatabase();
        }
    }

    /**
     * Close database
     */
    public void close () {
        database.close();
        database = null;
    }

    /**
     * Add data for favorited product
     *
     * @param productId int
     */
    public void addFavorite(int productId) {

        ContentValues values = new ContentValues();
        values.put(ProductDatabaseHelper.COLUMN_PRODUCT_ID, productId);
        database.insert(ProductDatabaseHelper.TABLE_FAVORITE, null, values);

    }

    /**
     * Remove data about favorited item
     *
     * @param productId int
     */
    public void deleteFavorite(int productId) {

        database.delete(
            ProductDatabaseHelper.TABLE_FAVORITE,
            ProductDatabaseHelper.COLUMN_PRODUCT_ID + " = " + productId,
            null
        );

    }

    /**
     * Check if product is in database
     *
     * @param productId int
     * @return boolean
     */
    public boolean isFavorite(int productId) {

        Cursor query = database.query(
            ProductDatabaseHelper.TABLE_FAVORITE,
            allColumns,
            ProductDatabaseHelper.COLUMN_PRODUCT_ID + " = ?",
            new String[]{String.valueOf(productId)},
            null,
            null,
            null
        );


        return query.getCount() > 0 ? true : false;
    }
}
