package com.example.android.productlist;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LCBOClient {

    private static final String BASE_URL = "http://lcboapi.com";
    private static final String ACCESS_KEY = "MDoyYmM5MjI4YS0wZjU3LTExZTctOTgzNC02MzNlMGQ4NDljMDU6TFJmb0Zrb2tNWmswVHN3ZVh4dDZrbnB4bG1JRnFVRVZvWk1h";
    private static AsyncHttpClient client = new AsyncHttpClient();
    private static ProductArrayList products = new ProductArrayList();
    private static int nextProductPage = 0;
    private RestView caller;
    private Product prod;

    /**
     * Initialize rest client
     *
     * @param context RestView
     */
    public LCBOClient(RestView context) {
        caller = context;
    }


    /**
     * Add RestView
     *
     * @param context RestView
     */
    public void setCaller(RestView context) { caller = context; }

    /**
     * Get call simplifier
     *
     * @param url String
     * @param params RequestParams
     * @param response AsyncHttpResponseHandler
     */
    public void get(String url, RequestParams params, AsyncHttpResponseHandler response) {
        client.get(getUrl(url), params, response);
    }

    /**
     * Post call simplifier
     *
     * @param url String
     * @param params RequestParams
     * @param response AsyncHttpResponseHandler
     */
    public void post(String url, RequestParams params, AsyncHttpResponseHandler response) {
        client.post(getUrl(url), params, response);
    }

    /**
     * Get full url from rest api urls
     *
     * @param url String
     * @return String
     */
    public String getUrl(String url) {
        return BASE_URL + url;
    }

    /**
     * Add authorization data to calls
     */
    public void authorize() {
        client.addHeader("Authorization", "Token token=" + ACCESS_KEY);
    }

    /**
     * Get product page
     */
    public void fetchProducts() {
        String productUrl = "/products";

        if (nextProductPage > 0) {
            productUrl += "?page=" + nextProductPage;
        }

        get(productUrl, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {}

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        int productCount = response.getJSONObject("pager").getInt("current_page_record_count");
                        for (int i = 0; i < productCount; i++) {
                            JSONObject product = response.getJSONArray("result").getJSONObject(i);
                            Product p = new Product(product.getInt("id"));
                            p.setName(product.getString("name"));
                            p.setImgUrl(product.getString("image_thumb_url"));
                            p.setPrice(product.getInt("price_in_cents"));
                            p.setDescription(product.getString("description"));
                            p.setDead(product.getBoolean("is_dead"));
                            p.addTags(product.getString("tags"));
                            p.setDiscontinued(product.getBoolean("is_discontinued"));
                            p.setRegularPrice(product.getInt("regular_price_in_cents"));
                            p.setCategory(product.getString("primary_category"));
                            p.setSecondaryCategory(product.getString("secondary_category"));
                            p.setOrigin(product.getString("origin"));
                            p.setProducer(product.getString("producer_name"));
                            p.setPackage(product.getString("package"));
                            p.setTotalPackageUnits(product.getInt("total_package_units"));
                            p.setServingSuggestion(product.getString("serving_suggestion"));
                            p.setTastingNote(product.getString("tasting_note"));
                            if (!products.contains(p)) {
                                products.add(p);
                            }
                        }
                        if (!response.getJSONObject("pager").getBoolean("is_final_page")) {
                            nextProductPage = response.getJSONObject("pager").getInt("next_page");
                        }
                    }
                } catch (JSONException ex) {
                    Log.e(this.getClass().getName(), "Result got is not in JSON!");
                }
                caller.refreshView();
            }
        });
    }

    /**
     * Get simple product
     *
     * @param id int
     * @return Product
     */
    public Product getProductById(int id) {

        prod = products.getById(id);

        if (prod != null) {
            return prod;
        }

        get("/products/" + id, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            try {
                if (response.getInt("status") == 200) {
                    Product p = new Product(response.getJSONObject("result").getInt("id"));
                    p.setName(response.getJSONObject("result").getString("name"));
                    p.setImgUrl(response.getJSONObject("result").getString("image_url"));
                    p.setDescription(response.getJSONObject("result").getString("desciption"));
                    p.setPrice(response.getJSONObject("result").getInt("price_in_cents"));
                    p.setDead(response.getJSONObject("result").getBoolean("is_dead"));
                    p.addTags(response.getJSONObject("result").getString("tags"));
                    p.setDiscontinued(response.getJSONObject("result").getBoolean("is_discuntinued"));
                    p.setRegularPrice(response.getJSONObject("result").getInt("regular_price_in_cents"));
                    p.setCategory(response.getJSONObject("result").getString("primary_category"));
                    p.setSecondaryCategory(response.getJSONObject("result").getString("secondary_category"));
                    p.setOrigin(response.getJSONObject("result").getString("origin"));
                    p.setPackage(response.getJSONObject("result").getString("package"));
                    p.setTotalPackageUnits(response.getJSONObject("result").getInt("total_package_units"));
                    p.setServingSuggestion(response.getJSONObject("result").getString("serving_suggestion"));
                    p.setTastingNote(response.getJSONObject("result").getString("tasting_note"));
                    prod = p;
                }
            } catch (JSONException ex) {
                Log.e(this.getClass().getName(), "Can't convert result to JSONObject!");
            }
            }
        });

        return prod;
    }

    /**
     * Add product array list
     *
     * @param pr ProductArrayList
     */
    public void setProductArrayList(ProductArrayList pr) { products = pr; }

    /**
     * Get product array list
     *
     * @return Product Array List
     */
    public ProductArrayList getProductArrayList() { return products; }

}
