package com.example.android.productlist;

import android.database.SQLException;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class ProductPage extends AppCompatActivity implements RestView{

    Product product;
    Favorites favorites = new Favorites(ProductPage.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);
        LCBOClient api = new LCBOClient(this);

        Bundle bundle = getIntent().getExtras();
        product = api.getProductById(bundle.getInt("product_id"));
        refreshView();

        FloatingActionButton fb = (FloatingActionButton) findViewById(R.id.btn_favorite);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try {
                favorites.openToWrite();
                if (favorites.isFavorite(product.getId())) {
                    favorites.deleteFavorite(product.getId());
                } else {
                    favorites.addFavorite(product.getId());
                }

                ((FloatingActionButton) v).setImageResource(
                        favorites.isFavorite(product.getId()) ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star_big_off
                );
                favorites.close();
            } catch (SQLException exception) {
                Log.e(this.getClass().getName(), "SQL Exception: " + exception.getMessage());
            }
            }
        });
    }

    /**
     * Refresh view after getting product data
     */
    @Override
    public void refreshView() {
        Glide.with(this)
            .load(product.getImgUrl())
            .centerCrop()
            .into((ImageView) findViewById(R.id.product_image));
        ((TextView) findViewById(R.id.product_name)).setText(product.getName());
        ((TextView) findViewById(R.id.product_description)).setText(product.getDescription());
        if (!product.getDescription().equals("null")) {
            ((TextView) findViewById(R.id.product_description)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.parameter_seperator)).setVisibility(View.VISIBLE);
        }
        ((TextView) findViewById(R.id.product_categories)).setText(product.getPrimaryCategory() + " / " + product.getSecondaryCategory());
        ((TextView) findViewById(R.id.product_producer)).setText(product.getProducer());
        ((TextView) findViewById(R.id.product_origin)).setText(product.getOrigin());
        ((TextView) findViewById(R.id.product_price)).setText(
            getString(R.string.price) + ": " + product.getPrice() / 100 + "." + product.getPrice() % 100 + " €"
        );
        try {
            favorites.openToRead();
            ((FloatingActionButton) findViewById(R.id.btn_favorite)).setImageResource(
                    favorites.isFavorite(product.getId()) ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star_big_off
            );
            favorites.close();
        } catch (SQLException exception) {
            Log.e(this.getClass().getName(), "SQL Exception: " + exception.getMessage());
        }
    }
}
