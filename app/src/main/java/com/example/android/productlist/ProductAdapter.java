package com.example.android.productlist;

import android.app.Activity;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.io.InputStream;

public class ProductAdapter extends ArrayAdapter<Product> {

    private Favorites favorites;

    public ProductAdapter(Activity context, ProductArrayList productList) {
        super(context, 0, productList);
        favorites = new Favorites(context);
    }

    /**
     * Create item for ListView
     *
     * @param position int
     * @param convertView View
     * @param parent ViewGroup
     * @return View
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false
            );
        }

        Product currentProduct = getItem(position);

        TextView productNameView = (TextView) listItemView.findViewById(R.id.product_name);
        productNameView.setText(currentProduct.getName());

        TextView productDescriptionView = (TextView) listItemView.findViewById(R.id.product_packaging);
        productDescriptionView.setText(currentProduct.getPackaging());

        TextView productPriceView = (TextView) listItemView.findViewById(R.id.product_price);
        productPriceView.setText(currentProduct.getPrice() / 100 + "." + currentProduct.getPrice() % 100 + " €");

        Glide.with(getContext())
            .load(currentProduct.getImgUrl())
            .override(100, 100)
            .fitCenter()
            .placeholder(R.mipmap.ic_launcher)
            .into((ImageView) listItemView.findViewById(R.id.product_image));

        try {
            favorites.openToRead();
            ((ImageView) listItemView.findViewById(R.id.product_favorite)).setImageResource(
                    favorites.isFavorite(currentProduct.getId()) ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star_big_off
            );
            favorites.close();
        } catch (SQLException exception) {
            Log.e(this.getClass().getName(), "SQL Exception: " + exception.getMessage());
        }
        return listItemView;
    }
}
